//
//  MySingleton.m
//  EasyQuiz
//
//  Created by diffy on 6/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MySingleton.h"
#import "Reachability.h"
#import "AppDelegate.h"

@implementation MySingleton

static MySingleton* _sharedMySingleton = nil;

@synthesize dbPath;

@synthesize fireFromFavorites,faveStringFromDB,feedFromFavArray,didModifyBookMark;

@synthesize pushedViewController;

+(MySingleton*)sharedMySingleton
{
	@synchronized([MySingleton class])
	{
		if (!_sharedMySingleton)
			[[self alloc] init];
        
		return _sharedMySingleton;
	}
    
	return nil;
}

-(void)dealloc
{
    [super dealloc];
}

+(id)alloc
{
	@synchronized([MySingleton class])
	{
		NSAssert(_sharedMySingleton == nil, @"Attempted to allocate a second instance of a singleton.");
		_sharedMySingleton = [super alloc];
		return _sharedMySingleton;
	}
    
	return nil;
}

-(id)init {
	self = [super init];
	if (self != nil) {
        fireFromFavorites = NO;
        didModifyBookMark = NO;
        pushedViewController = @"Home";
	}
    
	return self;
}
@end