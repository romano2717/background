//
//  AppDelegate.h
//  background
//
//  Created by diffy on 2/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import <AVFoundation/AVFoundation.h>


@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    Reachability* internetReachable;
    Reachability* hostReachable;
    
    UIBackgroundTaskIdentifier bgTask;
    
    AVAudioPlayer *audioPlayer;
}


@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@property (nonatomic, retain) AVAudioPlayer *audioPlayer;


-(void)initiateConnectionCheck;

@end
