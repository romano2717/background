//
//  AppDelegate.m
//  background
//
//  Created by diffy on 2/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"

#import "ViewController.h"

#import <AVFoundation/AVFoundation.h>

@implementation AppDelegate

@synthesize window = _window;
@synthesize viewController = _viewController;
@synthesize audioPlayer;

- (void)dealloc
{
    [_window release];
    [_viewController release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"03 - Heart In A Cage" ofType:@"mp3"]];
    
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    audioPlayer.numberOfLoops = -1;
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setActive: YES error: nil];
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    
    [audioPlayer prepareToPlay];
    
    [self initiateConnectionCheck];
    
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.
    self.viewController = [[[ViewController alloc] initWithNibName:@"ViewController" bundle:nil] autorelease];
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    NSLog(@"backgrounded");
    
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
    bgTask = [application beginBackgroundTaskWithExpirationHandler:^{
        // Clean up any unfinished task business by marking where you.
        // stopped or ending the task outright.
        [application endBackgroundTask:bgTask];
        bgTask = UIBackgroundTaskInvalid;
    }];
    
    // Start the long-running task and return immediately.
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        while (1) {
            NSLog(@"BGTime left: %f", [UIApplication sharedApplication].backgroundTimeRemaining);
            
            NSArray *oldNotifications         = [application scheduledLocalNotifications];
            NSLog(@"notif %@",oldNotifications);
            
            sleep(1);
        }
            
        
        [application endBackgroundTask:bgTask];
        bgTask = UIBackgroundTaskInvalid;
    });
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    NSLog(@"This was fired off");
}

//check internet connection
-(void)initiateConnectionCheck
{
    //kill the current bgtask and create a new one!
    bgTask = UIBackgroundTaskInvalid;
    
    UIBackgroundTaskIdentifier newTaskId = UIBackgroundTaskInvalid;
    
    newTaskId = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:NULL];
    
    // check for internet connection
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    
    internetReachable = [[Reachability reachabilityForInternetConnection] retain];
    [internetReachable startNotifier];
    
    // check if a pathway to a random host exists
    hostReachable = [[Reachability reachabilityWithHostName: @"www.google.com"] retain];
    [hostReachable startNotifier];
    
    //[self performSelector:@selector(foreverLog)];
    
    if (bgTask != UIBackgroundTaskInvalid) {
        [[UIApplication sharedApplication] endBackgroundTask: bgTask];
    }
    
    bgTask = newTaskId;
}

-(void)foreverLog
{
    NSLog(@"log");
    [self performSelector:@selector(foreverLog) withObject:nil afterDelay:3.0];
}

- (BOOL) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    
    NetworkStatus internetStatus = [internetReachable currentReachabilityStatus];
    
    
    UIApplication *app                = [UIApplication sharedApplication];
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    NSArray *oldNotifications         = [app scheduledLocalNotifications];
    
    if ([oldNotifications count] > 0) {
        [app cancelAllLocalNotifications];
    }
    
    if (notification == nil)
        return NO;
    
    NSDate *notificationDate = [NSDate dateWithTimeIntervalSinceNow:2];
    
    switch (internetStatus)
    
    {
        case NotReachable:
        {
            NSLog(@"NOT REACHABLE");
            
            notification.fireDate  = notificationDate;
            notification.timeZone  = [NSTimeZone systemTimeZone];
            notification.alertBody = @"Not Connected";
            //notification.soundName = @"alarm.caf";
            [app scheduleLocalNotification:notification];
            
            [audioPlayer play];
            
            return NO;
            break;
            
        }
        case ReachableViaWiFi:
        {
            NSLog(@"connected wifi");
            notification.fireDate  = notificationDate;
            notification.timeZone  = [NSTimeZone systemTimeZone];
            notification.alertBody = @"connected wifi";
            [app scheduleLocalNotification:notification];
            
            [audioPlayer pause];
            break;
            
        }
        case ReachableViaWWAN:
        {
            NSLog(@"connected WWAN");
            notification.fireDate  = notificationDate;
            notification.timeZone  = [NSTimeZone systemTimeZone];
            notification.alertBody = @"connected WWAN";
            [app scheduleLocalNotification:notification];
            
            [audioPlayer pause];
            break;
            
        }
    }
    [notification release];
    return NO;
}
@end
