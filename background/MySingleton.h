//
//  MySingleton.h
//  EasyQuiz
//
//  Created by diffy on 6/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface MySingleton : UIViewController {
    NSString *dbPath;
    
    BOOL fireFromFavorites;
    NSArray *feedFromFavArray;
    
    BOOL didModifyBookMark;
    
    NSString *pushedViewController;
}

@property(nonatomic, retain) NSString *dbPath;
@property(nonatomic, retain)NSString *pushedViewController;
@property (nonatomic) BOOL fireFromFavorites;
@property (nonatomic) BOOL didModifyBookMark;
@property (nonatomic, retain) NSMutableString *faveStringFromDB;
@property (nonatomic, retain)NSArray *feedFromFavArray;

+(MySingleton*)sharedMySingleton;

@end